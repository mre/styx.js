describe("styx.js Utilities Tests", function () {

    beforeEach(function () {
    });

    it("adds an each to arrays", function () {
        expect(typeof [].each).toBe('function');

        var arr = [1,2,3], sum = 0;

        arr.each(function () {
           sum += this;
        });

        expect(sum).toBe(6);
    });

    it("generates empty functions", function () {
        var fn = sx.emptyFn();

        expect(typeof fn).toBe('function');
        expect(fn()).toBe(undefined);
    });

    it("generates empty elements", function () {
        var el = sx.emptyEl();
        expect(el.length).toBe(1);
        expect(el.jquery).toBe('1.9.0');
    });

    it("can handle delays", function () {
        runs(function(){
            this.counter = 1;
            var self = this;
            sx.utils.delay('test', function () {
                self.counter++;
            }, 10);
            expect(this.counter).toBe(1);
        });
        waits(10);
        runs(function () {
            expect(this.counter).toBe(2);
        });
    });

    it("can set and get cookies", function () {
        sx.utils.cookie.set('testkey', 'testvalue');
        expect(sx.utils.cookie.get('testkey')).toBe('testvalue');
    });

});
