describe('styx.js RDFa Activation Tests', function () {
    var initializer;

    // dummy html holen
    beforeEach(function () {

        jasmine.getFixtures().fixturesPath = '/base/spec/fixtures';
        loadFixtures('styx-js.html');

        initializer = new sx.Initializer();

        sx.build({
            name: 'TestContent',
            nsp: 'com.plugins',
            parent: sx.Plugin,

            m: {
                counter: 0,

                init: function () {
                    this.counter++;
                }
            }
        });

        sx.build({
            name: 'OtherTestContent',
            nsp: 'com.plugins',
            parent: sx.Plugin,

            m: {
                counter: 0,

                init: function () {
                    this.counter++;
                }
            }
        });
    });

    it('references jQuery correctly', function () {
        expect($).toBeDefined();
    });

    it('loads the html template file correctly', function () {
        var nodeText = $('.load').text().trim();
        expect(nodeText).toBe('success-load');
    });

    it('retrieves all activated dom nodes via jquery', function () {
        var result = $('[typeof]');

        expect(result.length).toBe(4);
        expect(result.eq(0).text().trim()).toBe('success0');
    });

    it('retrieves the classnames from the typeof attribute', function () {
        expect(initializer.registry.length).not.toBe(0);
        expect(initializer.registry[0].name).toBe('com.plugins.TestContent');
    });

    it('checks correctly if the class exists', function () {

        var existingClassPluginObject = initializer.registry[0],
            notExistingClassPluginObject = {name: 'com.plugins.NotExisting'},
            existingExists = initializer.classExists(existingClassPluginObject),
            notExistingExists = initializer.classExists(notExistingClassPluginObject);

        expect(existingExists).toBeTruthy();
        expect(notExistingExists).toBeFalsy();
    });

    it('creates a valid instance of the found class', function () {

        var instance = initializer.createInstance(initializer.registry[0]);
        var notExistinginstance = initializer.createInstance({name: 'com.plugins.NotExisting'});

        expect(instance).toBeDefined();
        expect(instance.counter).toBe(1);
        expect(notExistinginstance).toBeUndefined();
    });

    it('creates instances of all found classes', function () {
        var instances = initializer.createInstances();

        expect(initializer.registry[0].instance).toBeDefined();
        expect(initializer.registry[1].instance).toBeDefined();
        expect(initializer.registry[2].instance).toBeUndefined();
    });

    it('creates distinct instances of all found classes', function () {
        var instances = initializer.createInstances();

        expect(initializer.registry[0].instance.counter).toBe(1);
        initializer.registry[0].instance.counter = 5;
        expect(initializer.registry[0].instance.counter).toBe(5);
        expect(initializer.registry[3].instance.counter).toBe(1);
    });

    it('assigns data-fields to the constructor', function () {
        var instances = initializer.createInstances();

        expect(initializer.registry[0].instance.testfield).toBe('success0');
        expect(initializer.registry[1].instance.testfield).toBe('success1');
    });

    it('asigns subElements to Plugins', function () {
        var plugin = initializer.registry[3].instance;

        expect(plugin).toBeDefined();

        expect(plugin.mySubElement).toBeDefined();
        expect(plugin.mySubElement.eq(0).text()).toBe('success3');
    });

    it('asigns more than one subElement per type', function () {
        var myplugin = initializer.registry[3].instance;

        expect(myplugin).toBeDefined();

        expect(myplugin.mySubElement).toBeDefined();
        expect(myplugin.mySubElement.length).toBe(2);
        expect(myplugin.mySubElement.eq(1).attr('class')).toBe('othersubelement');
    });

    /*** EVENT TRAIT ***/
    it('can handle event timed functions by adding the events trait', function () {
        sx.build({
            name: 'TestKlasse',
            nsp: 'com.styx',
            parent: sx.Helper,
            traits: [sx.Event],

            m: {
                testMethod: function () {
                }
            }
        });

        var testKlasse = new com.styx.TestKlasse();
        spyOn(testKlasse, 'testMethod');
        expect(testKlasse.on).toBeDefined(); //trait geladen
        expect(testKlasse.trigger).toBeDefined(); //trait geladen

        testKlasse.on('testEvent', testKlasse.testMethod);
        expect(testKlasse.events.testEvent).toBeDefined(); //event wurde registriert

        testKlasse.trigger('testEvent');
        expect(testKlasse.testMethod).toHaveBeenCalled();//event löste Methode aus
    });
});
