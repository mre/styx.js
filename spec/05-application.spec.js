/** @namespace wb.core.Plugin */
/** @namespace wb.core */

describe('styx.js Appliaction', function () {
    var target;
    var application;

    // dummy html holen
    beforeEach(function() {

        jasmine.getFixtures().fixturesPath = '/base/spec/fixtures';
        loadFixtures('styx-js-application.html');

        target = $('.target');

        sx.build({
            name: 'Div',
            nsp: 'com.plugins',
            parent: sx.Plugin,

            m: {
                init: function () {
                    target.text('success');
                }
            }
        });

        sx.build({
            name: 'LaunchHelper',
            nsp: 'com.helpers',
            parent: sx.Helper,

            m: {
                construct: function () {
                    $('body').append('<p />');
                }
            }
        });

        application = new sx.Application();
    });

    // kill the application
    afterEach(function() {
        application = null;
    });

    it('loads correctly', function () {
        expect(application).toBeDefined();
        expect(application.getClass()).toBe('sx.Application');
    });

    it('initializes all plugins', function () {
        expect(application.initializer.registry.length).toBe(1);
        expect(target.text()).toBe('success');
    });

    it('initializes all helpers', function () {
        application = new sx.Application(
            'com.helpers.LaunchHelper',
            'com.helpers.LaunchHelper'
        );

        expect($('body').find('p').length).toBe(2);
    });

});
