//noinspection JSUnresolvedFunction describe
//noinspection JSUnresolvedFunction beforeEach
describe("styx.js Basic Tests", function () {

    beforeEach(function () {
        sx.logging.resetLogstack();
    });

	it("defines a global namespace sx", function () {
        expect(typeof sx).not.toBe('undefined');
	});

    it("has a workaround for console.log", function () {
        expect(typeof window.console).not.toBe('undefined');
        expect(typeof window.console.log).not.toBe('undefined');
    });

    it("can define namespaces via sx.nsp()", function () {
        sx.nsp('path.to.namespace');

        expect(typeof path).not.toBe('undefined');
        expect(typeof path.to).not.toBe('undefined');
        expect(typeof path.to.namespace).not.toBe('undefined');
    });

    it("can throw an exception", function () {
        try{
            sx.exception('TestException', 'test message');
        } catch(msg) {
            expect(msg).toBe('TestException: test message');
        }
    });

});
