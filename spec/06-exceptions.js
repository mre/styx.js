describe('styx.js Initializer Exception Handling', function () {
    var initializer;

    // dummy html holen
    beforeEach(function() {

        jasmine.getFixtures().fixturesPath = '/base/spec/fixtures';


        sx.build({
            name: 'ExceptionClass1',
            nsp: 'com.plugins',
            parent: sx.Plugin,

            m: {
                param: 100
            }
        });

        sx.build({
            name: 'ExceptionClass2',
            nsp: 'com.plugins',
            parent: sx.Plugin,

            m: {
            }
        });
    });

    it('throws an exception if values already exist', function () {
        loadFixtures('styx-js-exception-param.html');
        initializer = new sx.Initializer();

        expect(initializer.registry.length).toBe(1);
        expect(initializer.registry[0].instance).toBeUndefined();
    });

    it('throws an exception if values already exist', function () {
        loadFixtures('styx-js-exception-el.html');
        initializer = new wb.core.Initializer();

        expect(initializer.registry.length).toBe(1);
        expect(initializer.registry[0].instance).toBeUndefined();
    });

    it('throws an exception if values already exist', function () {
        loadFixtures('styx-js-exception-param-el.html');
        initializer = new wb.core.Initializer();

        expect(initializer.registry.length).toBe(1);
        expect(initializer.registry[0].instance).toBeUndefined();
    });
});
